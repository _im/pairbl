class v2
  constructor : (@x, @y) ->
  dist : (b) ->
    Math.sqrt(Math.pow(b.x - @x, 2) + Math.pow(b.y - @y, 2))

class uv
  constructor : (@a, @b) ->
    @vs = [
      new v2(@a.x,@a.y)
      new v2(@b.x,@a.y)
      new v2(@b.x,@b.y)
      new v2(@a.x,@b.y)
    ]
    @w = (@vs[1].x - @vs[0].x)
    @h = (@vs[2].y - @vs[0].y)

class doll
  setcenter : () ->
    @width = @uv.w * (SIZE.w / innerWidth)
    @height = @uv.h * (SIZE.h / innerHeight)
    @center = new v2(@pos.x + @width * 0.5, @pos.y + @height * 0.5)
  constructor : (@source, @side, @pos, @uv) ->
    @name = @source
    @setcenter()
  setpos : (x, y) ->
    @pos.x = x
    @pos.y = y
    @setcenter()
  is : (d) ->
    @side is d.side and @name is d.name
