clipping = true

vs = []
current = null
left = true

startpos = new v2(0,0)
drawing = false
selectingcrop = false

chromakey = null

preload = () ->
  chromakey = loadShader("shader/default.vert", "shader/chroma.frag")

newcrop = () ->
  uvs : [ new uv(new v2(0,0), new v2(0,0)), new uv(new v2(0,0), new v2(0,0)) ]
  crop : new uv(new v2(0.23, 0.1), new v2(0.78, 0.93))

createCrops = () ->
  for v, i in VIDEOS
    newcrop()

squareuv = (x, y) ->
  w = 0.5
  r = 16 / 9
  h = w * r
  x = x / innerWidth
  y = y / (innerWidth * r)
  new uv(new v2(x, y), new v2(x + w, y + h))

uvtocropinput = (uv, w, h) ->
  x : uv.a.x * w
  y : uv.a.y * h
  w : (uv.b.x - uv.a.x) * w
  h : (uv.b.y - uv.a.y) * h

getcrops = () ->
  vs.map((v)->
    c = uvtocropinput v.crop, v.video.width, v.video.height
    {
      name : v.name
      crop : "#{Math.floor(c.w)}:#{Math.floor(c.h)}:#{Math.floor(c.x)}:#{Math.floor(c.y)}"
    }
  )


setup = () ->
  createCanvas(innerWidth, innerWidth, WEBGL)

  vs = for name, i in VIDEOS
    v = createVideo("raw/cropped/#{name}.mp4")
    v.stop()
    # v.loop()
    v.hide()
    o = CROPS[i]
    o.video = v
    o.playing = false
    o.name = name
    o 
  current = vs[0]

  $("canvas").mousedown((e) ->
    drawing = true
    x = mouseX / width
    y = mouseY / width
    startpos = new v2(x, y)
    if selectingcrop
      current.crop = squareuv(e.offsetX, e.offsetY)
  )
  $("canvas").mouseup((e) ->
    drawing = false
  )

drawuv = (c, uvs) ->
  push()
  fill(c)
  stroke(c)
  noFill()
  beginShape()
  vertex(uvs.a.x * width, uvs.a.y * height, 0)
  vertex(uvs.a.x * width + uvs.w * width, uvs.a.y * height, 0)
  vertex(uvs.a.x * width + uvs.w * width, uvs.a.y * height + uvs.h * height, 0)
  vertex(uvs.a.x * width, uvs.a.y * height + uvs.h * height, 0)
  endShape(CLOSE)
  pop()
  

draw = () ->
  background("#0002")
  translate(- width * 0.5, - height * 0.5)
  chromakey.setUniform("screenColor", color(COLORS.chroma).levels.reduce(((a, v, i) -> if i < 3 then a.concat([v / 255]) else a), []))

  shader(chromakey)
  texture(current.video)
  beginShape()
  vertex(0, 0, 0, 0, 0)
  vertex(width, 0, 0, 1, 0)
  vertex(width, height, 0, 1, 1)
  vertex(0, height, 0, 0, 1)
  endShape()

  uvs = 
    if drawing 
      currentpos = new v2(mouseX / width, mouseY / width)
      current.uvs[(if left then 0 else 1)] = new uv(startpos, currentpos)
      current.uvs[(if left then 0 else 1)]
    else
      current.uvs[(if left then 0 else 1)]

  
  drawuv COLORS.left, current.uvs[0]
  drawuv COLORS.right, current.uvs[1]

  # push()
  # c = "#fff"
  # fill(c)
  # stroke(c)
  # noFill()
  # beginShape()
  # vertex(current.crop.a.x * width, current.crop.a.y * height, 0)
  # vertex(current.crop.a.x * width + current.crop.w * width, current.crop.a.y * height, 0)
  # vertex(current.crop.a.x * width + current.crop.w * width, current.crop.a.y * height + current.crop.h * height, 0)
  # vertex(current.crop.a.x * width, current.crop.a.y * height + current.crop.h * height, 0)
  # endShape(CLOSE)
  # pop()


$(
  "<div>", 
  {
    id : "#buttons", 
    html : VIDEOS.map(
      (v, i) -> 
        $(
          "<button>", 
          {
            html : v, 
            id : "v#{i}"
          }
        ).click(
          (e) -> 
            id = parseInt(e.currentTarget.id.substring(1, e.currentTarget.id.length), 10)
            console.log id
            if current.playing
              current.playing = false;
              current.video.stop()
            current = vs[id]
        )
    )
  }
).appendTo($("body"))

selectcrop = (e) ->
  selectingcrop = true

selectside = (e) ->
  selectingcrop = false
  left = e.currentTarget.innerHTML is "left"
  $(".selected").css("background-color", "#666").removeClass "selected"
  $(this).addClass "selected"
  $(this).css("background-color", (if left then COLORS.left else COLORS.right))

$("<button>",{id : "left", html:"left"})
  .click( selectside)
  .appendTo($("body"))
  .css("border", "2px solid #{COLORS.left}")

$("<button>",{id : "right", html:"right"})
  .click( selectside)
  .appendTo($("body"))
  .css("border", "2px solid #{COLORS.right}")

$("<button>",{id : "crop", html:"crop"})
  .click( selectcrop )
  .appendTo($("body"))
  .css({border: "2px solid #{COLORS.right}", left: "100px"})

$("<textarea>",{id : "output"}).appendTo($("body"))



$("html").keyup(
  (e) -> 
    if e.key is " "
      if current.playing
        current.playing = false
        current.video.pause()
      else 
        current.video.play()
        current.playing = true
    else if e.key is "Enter"
      json = JSON.stringify(vs.map((v) -> {uvs:v.uvs}))
      $("#output").val(json)
      localStorage.setItem("CROPS", json)
)

