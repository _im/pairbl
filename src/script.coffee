sources = {}

VIDEOS.map((v, i) -> sources[v] = CROPS[i])

uvlist = [
  new uv(new v2(0.24, 0.11), new v2(0.5, 0.9))
  new uv(new v2(0.5, 0.11), new v2(0.7, 0.9))
]

chromakey = null

t = 0
r = 0.1
last = 0
selected = null
picked = null
pickedId = -1
showingEndButton = false
showingResults = false
offset = new v2(0, 0)

pairs = []

dolls = Object.keys(sources).map((k) -> (uvs : sources[k].uvs, name : k)).reduce(((a, s, i) ->
  a.concat [new doll(
    s.name,
    "left", 
    new v2(
      Math.random() * (1 - s.uvs[0].w * (SIZE.w / innerWidth)), 
      Math.random() * (1 - s.uvs[0].h * (SIZE.h / innerHeight))
    ),
    s.uvs[0]
  ),
  new doll(
    s.name,
    "right", 
    new v2(
      Math.random() * (1 - s.uvs[1].w * (SIZE.w / innerWidth)), 
      Math.random() * (1 - s.uvs[1].h * (SIZE.h / innerHeight))
    ),
    s.uvs[1]
  )]), []
)

collides = (p, d) ->
  d.pos.x < p.x < d.pos.x + d.uv.w && d.pos.y < p.y < d.pos.y + d.uv.h

draw_doll = (t, x, y, crop, w = 1, h = 1) ->
  texture t
  beginShape()
  vertex x * width, y * height, 0, crop.vs[0].x, crop.vs[0].y
  vertex x * width + crop.w * SIZE.w * w, y * height, 0, crop.vs[1].x, crop.vs[1].y
  vertex x * width + crop.w * SIZE.w * w, y * height + crop.h * SIZE.h * h, 0, crop.vs[2].x, crop.vs[2].y
  vertex x * width, y * height + crop.h * SIZE.h * h, 0, crop.vs[3].x, crop.vs[3].y 
  endShape CLOSE

draw_pair = (p) ->
  fill "#0555"
  stroke "#0005"
  # strokeCap PROJECT
  strokeWeight 20
  noFill()
  beginShape LINES
  vertex p.a.center.x * width, p.a.center.y * height + p.a.height * 0.42 * height, 0
  vertex p.b.center.x * width, p.b.center.y * height + p.b.height * 0.42 * height, 0
  endShape()

getscore = (ps) ->
  wrong = 0
  for p in ps
    if p.a.name isnt p.b.name
      wrong++
      sources[p.a.name].video.pause()
      sources[p.b.name].video.pause()
  ps.length / 2 - wrong / 2

findpairs = () ->
  threshold = 0.3
  for d, i in dolls
    d.closest = null
    closest = dolls.reduce(((a, v) ->
        if( a.center.dist(d.center) > v.center.dist(d.center) and not d.is(v)) then v else a),
        dolls[(i + 1) % dolls.length]
    )
    if closest.center.dist(d.center) < threshold
      d.closest = closest
  
  
  pairs = []
  allpaired = true
  for d, i in dolls
    if d.closest? and d.closest.closest? and d.closest.closest.is d
      pairs.push(a : d, b : d.closest)
    else
      allpaired = false
      d.closest = null

  allpaired

enablevideos = () ->
  for k, v of sources
    v.video.loop()

preload = () ->
  chromakey = loadShader("shader/default.vert", "shader/chroma.frag")
  Object.keys(sources).map((k) ->
    video = createVideo("scaled/#{k}.mp4")
    # video.loop()
    video.hide()
    sources[k].video = video
  )

endText = (s, a) ->
  if s == 0
    "Have you even tried?"
  else if s < 3
    "Look closer!"
  else if s < 7
    "Well, it is a good start."
  else if s < 9
    "Not bad!"
  else if s < 11
    "Good job!"
  else if s < 13
    "Almost perfect!"
  else
    "Wow, you are amazing!"

bindhelp = (b) ->
  $(b)
    .html("?")
    .mouseenter(() -> $(b).html(HELP))
    .mouseleave(() -> $(b).html("?"))
    .click(() -> $(b).html(HELP))

bindending = (b) ->
  $(b)
    .off "mouseenter mouseleave click"
    .html "all paired! show results?"
    .click () -> 
      showingResults = true
      score = getscore pairs
      all = dolls.length / 2
      $(@)
        .click () -> window.location.reload()
        .html endText(score, all) + "<br>You matched #{score} out of #{all}! <br>Try again?"

setup = () ->
  createCanvas(innerWidth, innerHeight, WEBGL)
  findpairs()
  $("<button>",
    id:"start-button" 
    html : "PLAY"
    click: () -> 
      $("#start-button").html("").animate({left : 0, top : 0, margin : 0}, 400, () -> bindhelp @)
      $("canvas").animate({opacity : 1}, 700)
      enablevideos()
  ).appendTo($("body"))
  $(window).resize () -> window.location.reload()

  # gui = new dat.GUI()
  # gui.addColor(COLORS, "chroma").onChange((c) -> chromakey.setUniform("screenColor", color(c).levels.reduce(((a, v, i) -> if i < 3 then a.concat([v / 255]) else a), [])))

draw_bounds = (d, c) ->
  fill c
  rect d.pos.x * width, d.pos.y * height, d.width * width, d.height * height

draw_score = (s, a) ->
  for p in pairs
    if p.a.name is p.b.name
      draw_bounds p.a, "#0555"
      draw_bounds p.b, "#0555"
    # if p.a.name isnt p.b.name
    #   draw_bounds p.a, "#0009"
    #   draw_bounds p.b, "#0009"

draw = () ->
  delta = (millis() - last) * 0.001
  chromakey.setUniform("screenColor", color(COLORS.chroma).levels.reduce(((a, v, i) -> if i < 3 then a.concat([v / 255]) else a), []))
  last = millis()
  t = (t + r * delta)
  if t > 1 then t = t - 1.0
  translate(-width * 0.5, -height * 0.5)
  background("#3333")
  mp = new v2(mouseX / width, mouseY / height)

  colliding = dolls.map((d, i) -> i).filter((d) -> (collides mp, dolls[d])) 

  if not showingResults
    push()
    if colliding.length > 0 and not picked?
      selected = dolls[colliding.reduce(((a, v) ->
          if dolls[a].center.dist(mp) > dolls[v].center.dist(mp) then v else a),
        colliding[0]
      )]
      draw_bounds selected, "#0005"

    if picked?
      if pickedId isnt dolls.length - 1 or pickedId isnt - 1
        dolls.splice pickedId, 1
        dolls.push picked
        pickedId = dolls.length - 1
      # dolls.sort((a, b) -> a.pos.y < b.pos.y)

      picked.setpos(mp.x - offset.x, mp.y - offset.y)
      draw_bounds picked, "#0555"
  
      if findpairs()
        if not showingEndButton
          bindending($("#start-button"))
          showingEndButton = true
      else if showingEndButton
        bindhelp($("#start-button"))
        showingEndButton = false  

    if pairs.length > 0
      for p in pairs
        draw_pair p
    pop()
  else
    draw_score getscore pairs

  shader(chromakey)

  for d in dolls
    draw_doll(sources[d.source].video, d.pos.x, d.pos.y, d.uv)

mousePressed = (e) ->
  if not showingResults and selected?
    picked = selected
    for d, i in dolls
      if d.is selected
        pickedId = i
    offset.x = mouseX / width - selected.pos.x
    offset.y = mouseY / height - selected.pos.y

mouseReleased = () ->
  if not showingResults and picked? then picked = null
